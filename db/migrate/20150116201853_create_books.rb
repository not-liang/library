class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.references :author, index: true
      t.references :genre, index: true
      t.references :publisher, index: true
      t.text :title
      t.text :isbn
      t.float :price
      t.integer :amount
      t.integer :remaining
      t.text :description
      t.integer :edition
      t.text :image
      t.integer :popularity

      t.timestamps
    end
  end
end
