class BooksController < ApplicationController
  before_action :authenticate, except: [:index, :show, :search, :genre_search, :ranking]
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show, :search, :genre_search, :ranking]

  respond_to :html

  def index
    @books = Book.all.order(:title)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @books.to_xml(:include => [:author, :genre, :publisher]) }
      format.json  { render :json => @books.to_json(:include => [:author, :genre, :publisher]) }
    end
  end

  def show
  end

  def new
    @book = Book.new
    respond_with(@book)
  end

  def edit
  end

  def create
    @book = Book.new(book_params)
    @book.save
    respond_with(@book)
  end

  def update
    @book.update(book_params)
    respond_with(@book)
  end

  def destroy
    @book.destroy
    respond_with(@book)
  end

  def search
    @books = Book.all.joins(:author, :genre).where("books.author_id = authors.id and books.genre_id = genres.id and ( lower(title) like ? or lower(authors.name) like ? or isbn like ? )", "%#{params[:keyword].downcase}%", "%#{params[:keyword].downcase}%", "%#{params[:keyword].downcase}%").order(:title).includes(:author, :genre)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @books.to_xml(:include => [:author, :genre, :publisher]) }
      format.json  { render :json => @books.to_json(:include => [:author, :genre, :publisher]) }
    end
  end

  def genre_search
    @books = Book.all.joins(:genre).where("books.genre_id = genres.id and genres.name like ? ", "%#{params[:keyword]}%").order(:title).includes(:genre)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @books.to_xml(:include => [:author, :genre, :publisher]) }
      format.json  { render :json => @books.to_json(:include => [:author, :genre, :publisher]) }
    end
  end

  def ranking
    @books = Book.all.order(popularity: :desc)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @books.to_xml(:include => [:author, :genre, :publisher]) }
      format.json  { render :json => @books.to_json(:include => [:author, :genre, :publisher]) }
    end
  end

  private
    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:author_id, :genre_id, :publisher_id, :title, :isbn, :price, :amount, :remaining, :desciption, :edition, :image, :popularity)
    end
    
    def authenticate
      if !current_user.try(:admin?)
        flash[:error] = "You don't have a permission!"
        redirect_to :root
      end
    end
end
