json.extract! @book, :id, :author_id, :genre_id, :publisher_id, :title, :isbn, :price, :amount, :remaining, :description, :edition, :image, :popularity, :created_at, :updated_at
