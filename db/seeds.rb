# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.new(email: 'admin@library.co.jp', password: '12345678', password_confirmation: '12345678', name: 'Administrator', address: 'On the internet', max_book: '10', borrowed: '0', user_type: '0')
user.admin = true
user.confirmed_at = Time.now
user.save
user = User.new(email: 'tanakom.b@ruri.waseda.jp', password: 'password123', password_confirmation: 'password123', name: 'Tanakom Banchaditt', address: 'Kitakyushu, Japan', max_book: '10', borrowed: '0', user_type: '0')
user.confirmed_at = Time.now
user.save

genre = Genre.create(name: 'Drama')
genre = Genre.create(name: 'Romance')
genre = Genre.create(name: 'Tragedy')
genre = Genre.create(name: 'Mystery')
genre = Genre.create(name: 'Crime')
genre = Genre.create(name: 'Horror')
genre = Genre.create(name: 'Fairy tale')
genre = Genre.create(name: 'Fantasy')
genre = Genre.create(name: 'Comedy')
genre = Genre.create(name: 'Comic')
genre = Genre.create(name: 'Dictionary')
genre = Genre.create(name: 'Biography')
genre = Genre.create(name: 'History')
genre = Genre.create(name: 'Magazine')

publisher = Publisher.create(name: 'Elsevier')
publisher = Publisher.create(name: 'Pearson Education')
publisher = Publisher.create(name: 'Apress')
publisher = Publisher.create(name: 'B & W Publishing')
publisher = Publisher.create(name: 'Book Works')
publisher = Publisher.create(name: 'CRC Press')
publisher = Publisher.create(name: 'Greenwood Publishing Group')
publisher = Publisher.create(name: 'Longman')
publisher = Publisher.create(name: 'Pathfinder Press')

status = Status.create(name: 'Waiting for Payment')
status = Status.create(name: 'Payment Received')
status = Status.create(name: 'Shipping')
status = Status.create(name: 'Received')
status = Status.create(name: 'Lost')
status = Status.create(name: 'Overdue')
status = Status.create(name: 'End')

author = Author.create(name: 'Ahmet Zappa')
author = Author.create(name: 'Alan Coren')
author = Author.create(name: 'Arundhati Roy')
author = Author.create(name: 'Beatrix Potter')
author = Author.create(name: 'Dante Alighieri')
author = Author.create(name: 'Ellen Hopkins')
author = Author.create(name: 'Frank Miller')
author = Author.create(name: 'Georges Simenon')
author = Author.create(name: 'Harlan Ellison')
author = Author.create(name: 'Isaac Asimov')
author = Author.create(name: 'J. K. Rowling')
author = Author.create(name: 'Lee Child')
author = Author.create(name: 'Martin Amis')
author = Author.create(name: 'Milan Kundera')

for i in 1..10
	author = Author.new
	author.name = 'Anonymous #' + i.to_s
	author.save
end

for i in 1..80
	book = Book.new
	book.genre_id = ( i % 14 ) + 1
	book.author_id = ( i % 24 ) + 1
	book.publisher_id = ( i % 9 ) + 1
	if (i <= 10)
		book.title = 'Novel #' + i.to_s
		book.genre_id = ( i % 8 ) + 1
	elsif (i <= 20)
		book.title = 'Dictionary #' + ((i % 10) + 1).to_s
	elsif (i <= 30)
		book.title = 'Text book #' + ((i % 4) + 11).to_s
		book.genre_id = ( i % 8 ) + 1
	elsif (i <= 40)
		book.title = 'Book #' + ((i % 10) + 1).to_s
	elsif (i <= 50)
		book.title = 'Magazine #' + ((i % 10) + 1).to_s
	else
		o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
		string = (0...4).map { o[rand(o.length)] }.join
		book.title = 'Secret book #' + string
	end
	book.isbn = '0-7475-32' + i.to_s + '-9'
	book.price = 100
	book.amount = (i % 5) + 1
	book.remaining = (i % 5) + 1
	book.description = 'This is an example description no.' + i.to_s + ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices semper libero et pretium. Donec interdum velit metus, ac volutpat magna sagittis vel. Sed tristique eu turpis nec gravida. Proin aliquam massa sapien, vitae vehicula ex egestas vel. Ut tempor condimentum odio, non venenatis ex. Nullam volutpat maximus hendrerit. In mattis metus at fermentum hendrerit. Quisque vitae turpis aliquet, sagittis magna sed, viverra elit. Pellentesque sed ornare odio, in fermentum quam. Aliquam ut dapibus risus, sit amet aliquet lorem. Maecenas eu justo sodales, hendrerit purus sit amet, bibendum ipsum. Nulla quis sapien eu velit fermentum tempor interdum eu ex. Nullam a lobortis odio. Pellentesque congue elit sit amet massa pharetra tincidunt. Proin risus velit, blandit sit amet rutrum id, sodales in elit. Ut neque magna, rhoncus venenatis lacus sed, auctor sodales urna. '
	book.edition = 1
	book.image = ''
	book.popularity = i % 24
	book.save
end