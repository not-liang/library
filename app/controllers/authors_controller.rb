class AuthorsController < ApplicationController
  before_action :authenticate, except: [:index, :show]
  before_action :set_author, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  respond_to :html

  def index
    @authors = Author.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @authors }
      format.json  { render :json => @authors }
    end
  end

  def show
  end

  def new
    @author = Author.new
    respond_with(@author)
  end

  def edit

  end

  def create
    @author = Author.new(author_params)
    @author.save
    respond_with(@author)
  end

  def update
    @author.update(author_params)
    respond_with(@author)
  end

  def destroy
    @author.destroy
    respond_with(@author)
  end

  private
    def set_author
      @author = Author.find(params[:id])
    end

    def author_params
      params.require(:author).permit(:name)
    end

    def authenticate
      if !current_user.try(:admin?)
        flash[:error] = "You don't have a permission!"
        redirect_to :root
      end
    end
end
