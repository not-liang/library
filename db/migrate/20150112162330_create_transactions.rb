class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :book, index: true
      t.references :user, index: true, :class_name => "User" 
      t.references :staff, index: true, :class_name => "User" 
      t.references :status, index: true
      t.date :issue_date
      t.date :due_date

      t.timestamps
    end
  end
end
