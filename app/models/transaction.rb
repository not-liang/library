class Transaction < ActiveRecord::Base
  belongs_to :book
  belongs_to :user, :class_name => "User"
  belongs_to :staff, :class_name => "User"
  belongs_to :status
end
