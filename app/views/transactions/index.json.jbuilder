json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :book_id, :user_id, :staff_id, :status_id, :issue_date, :due_date
  json.url transaction_url(transaction, format: :json)
end
