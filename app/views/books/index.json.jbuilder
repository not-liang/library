json.array!(@books) do |book|
  json.extract! book, :id, :author_id, :genre_id, :publisher_id, :title, :isbn, :price, :amount, :remaining, :description, :edition, :image, :popularity
  json.url book_url(book, format: :json)
end
