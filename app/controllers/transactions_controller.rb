class TransactionsController < ApplicationController
  before_action :authenticate, except: [:index, :show, :rent]
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /transactions
  # GET /transactions.json
  def index
    if current_user.try(:admin?)
      @transactions = Transaction.all.order(issue_date: :desc)
    else
      @transactions = Transaction.all.where("user_id =?", current_user.id).order(issue_date: :desc)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @transactions.to_xml(:include => [:book, :user, :status]) }
      format.json  { render :json => @transactions.to_json(:include => [:book, :user, :status]) }
    end
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @transactions.to_xml(:include => [:book, :user, :status]) }
      format.json  { render :json => @transactions.to_json(:include => [:book, :user, :status]) }
    end
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    if @transaction.status_id < 7 # Created Transaction's Book is not returned
      validate_book
      validate_user

      book = Book.where(id: transaction_params[:book_id]).first
      book.decrement!(:remaining)
      user = User.where(id: transaction_params[:user_id]).first
      user.increment!(:borrowed)
    end

    respond_to do |format|
      if @transaction.save        
        book.increment!(:popularity)
        format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        if @transaction.status_id = '7' # Book is returned
          book = Book.where(id: transaction_params[:book_id]).first
          book.increment!(:remaining)
          user = User.where(id: transaction_params[:user_id]).first
          user.decrement!(:borrowed)
        end
        format.html { redirect_to @transaction, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    if @transaction.status_id != '7' # Book is returned
      user = User.where(id: @transaction.user_id).first
      user.decrement!(:borrowed)
    end

    Book.where(id: @transaction.book_id).first.increment!(:remaining)
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def rent
    is_book_valid = validate_book
    is_user_valid = validate_user

    if is_book_valid == false && is_user_valid == false
      errors = ['This book is not available!', 'You have reached a limit for borrowing books.']
      flash[:error] = errors.join("<br/>").html_safe
    elsif is_book_valid == false
      flash[:error] = "This book is not available!"
    elsif is_user_valid == false
      flash[:error] = "You have reached a limit for borrowing books."
    end

    if is_book_valid == true && is_user_valid == true
      @transaction = Transaction.new(transaction_params)
      @transaction.staff_id = '1'
      @transaction.status_id = '1' # Waiting for payment
      today = Time.now
      @transaction.issue_date = today.strftime("%Y-%m-%d")
      @transaction.due_date = (today + 10.day).strftime("%Y-%m-%d")

      respond_to do |format|
        if @transaction.save
          book = Book.where(id: transaction_params[:book_id]).first
          book.decrement!(:remaining)
          book.increment!(:popularity)
          user = User.where(id: transaction_params[:user_id]).first
          user.increment!(:borrowed)
          flash[:notice] = "Transaction was successfully created."
          format.html { redirect_to @transaction, notice: 'Transaction was successfully created.' }
          format.json { render :show, status: :created, location: @transaction }
        else
          format.html { render :new }
          format.json { render json: @transaction.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:book_id, :user_id, :staff_id, :status_id, :issue_date, :due_date)
    end

    def authenticate
      if !current_user.try(:admin?)
        flash[:error] = "You don't have a permission!"
        redirect_to :root
      end
    end

    def validate_book
      book_check = Book.where(id: transaction_params[:book_id]).first
      if (book_check.remaining == 0)
        return false
      else
        return true
      end
    end

    def validate_user
      user_check = User.where(id: transaction_params[:user_id]).first
      if (user_check.increment(:borrowed).borrowed > user_check.max_book)
        return false
      else
        return true
      end
    end
end
