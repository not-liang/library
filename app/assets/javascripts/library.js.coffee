window.Application ||= {}

jQuery ->
  sb = $("#on-search-page")
  $(document).on 'keyup.myPlugin', '#on-search-page', (e) ->
    if e.which == 13
      callback = (response) -> 
        $('#search-result').empty()
        for obj in response
          id = obj.id
          title = obj.title
          edition = obj.edition
          author = obj.author.name
          isbn = obj.isbn

          tag = "<li class='row-fluid'> <div class='myicon'> <b> " + title + " </b>, Edition " + edition + " by <i class='hidden-xs'> " + author + "</i> <span class='hidden-xs hidden-sm hidden-md'> (isbn: " + isbn + ") </span> </div> <div class='pull-right'> <a href='/books/" + id + "' class='btn btn-xs' role='button'><span class='glyphicon glyphicon-zoom-in'></span></a></div></li>"
          $('#search-result').append(tag)
      if sb.val().length == 0
        $.get '/books.json', {}, callback, 'json'
      else
        $.get '/search/' + sb.val() + '.json', {}, callback, 'json'

jQuery ->
  sb = $("#off-search-page")
  $(document).on 'keyup.myPlugin', '#off-search-page', (e) ->
    if e.which == 13
      window.location.replace('/search/' + $("#off-search-page").val())

jQuery ->
  $(document).on 'click', 'ul.genre-list a.on-genre-search-page', (e) ->
    g = $(this).find('#clicked-genre').text()
    callback = (response) -> 
      $('#search-result').empty()
      for obj in response
        id = obj.id
        title = obj.title
        edition = obj.edition
        author = obj.author.name
        isbn = obj.isbn

        tag = "<li class='row-fluid'> <div class='myicon'> <b> " + title + " </b>, Edition " + edition + " by <i class='hidden-xs'> " + author + "</i> <span class='hidden-xs hidden-sm hidden-md'> (isbn: " + isbn + ") </span> </div> <div class='pull-right'> <a href='/books/" + id + "' class='btn btn-xs' role='button'><span class='glyphicon glyphicon-zoom-in'></span></a></div></li>"
        $('#search-result').append(tag)
    if g == "All"
      $.get '/books.json', {}, callback, 'json'
    else
      $.get '/genre_search/' + g + '.json', {}, callback, 'json'

jQuery ->
  $(document).on 'click', 'ul.genre-list a.off-genre-search-page', (e) ->
    g = $(this).find('#clicked-genre').text()
    if g == 'All'
      window.location.replace('/books')
    else
      window.location.replace('/genre_search/' + g)

jQuery ->
  $(document).on 'click', '#rent-btn', (e) ->
    data = $(this).attr("value")
    tmp = data.split("|")
    thisbook = tmp[0]
    thisuser = tmp[1]
    $.ajax
      url: '/rent'
      type: 'POST'
      dataType: 'json'
      data: { transaction: { book_id: thisbook, user_id: thisuser } }
      error: (jqXHR, textStatus, errorThrown) ->
        console.log(textStatus)
        location.reload(); 
      success: (data, textStatus, jqXHR) ->
        console.log(textStatus)
        window.location.replace('/transactions/' + data.id)
###
jQuery ->
  $(document).on 'click', '#show-book-info', (e) ->
    thisbook = $(this).attr("value")
    $.ajax "/books/" + thisbook,
      type: "GET",
      dataType: "json",
      success: (data) ->
        console.log(data)
        tag = "<p> <strong> Title: </strong>" + data.title + "</p>" + "<p> <strong> Edition: </strong>" + data.edition + "</p>" + "</p>" + "<p> <strong> Isbn: </strong>" + data.isbn + "</p>" + "</p>" + "<p> <strong> Author: </strong>" + data.author.name + "</p>" + "</p>" + "<p> <strong> Publisher: </strong>" + data.publisher.name + "</p>"
        $('#book-detail').append(tag)      
###